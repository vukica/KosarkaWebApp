var wafepaApp = angular.module("kosarka", ['ngRoute']);

wafepaApp.controller("ctrl", function($scope){


});
wafepaApp.controller("igraciCtrl", function($scope, $http, $location){

	var baseUrl = "/api/igraci";
	var baseUrlTimovi = "/api/timovi"
	var baseUrlPozicije="/api/pozicije";
	$scope.timovi = [];
	$scope.pozicije = [];
	$scope.igraci = [];
	$scope.igrac = {};
	$scope.igrac.imePrezime = "";
	$scope.igrac.broj = "";
	$scope.igrac.licne = "";
	$scope.igrac.pozicijaId = 0;
	$scope.igrac.timId = 0;


	//=====pretraga=========
	$scope.search = {};
	$scope.search.imePrezime = "";
	$scope.search.broj = "";
	$scope.search.timId = "";


	$scope.pageNum = 0;
	$scope.totalPages = 1;
	
	
	var getIgraci = function(){
		
	var config = { params: {} };
		
		if($scope.search.imePrezime != ""){
			config.params.imePrezime = $scope.search.imePrezime;
		}
		if($scope.search.godOd != ""){
			config.params.broj = $scope.search.broj;
		}
		if($scope.search.potDo != ""){
			config.params.timId = $scope.search.timId;
		}
		config.params.pageNum = $scope.pageNum;
		
		$http.get(baseUrl,config).then(
				function success(res){
					$scope.igraci = res.data;
					$scope.totalPages = res.headers("totalPages");
				},
				function error(res){
					alert("Nisu se dobavili igraci!");
				}
		);		
	}
	getIgraci();
	

	var getPozicije = function(){
		$http.get(baseUrlPozicije).then(
				function success(res){
					$scope.pozicije = res.data;
				},
				function error(res){
					alert("Nisu se dobavile pozicije!");
				}
		);
	}
	getPozicije();

	var getTimovi= function(){
		$http.get(baseUrlTimovi).then(
				function success(res){
					$scope.timovi = res.data;
				},
				function error(res){
					alert("Nisu se dobavili timovi!");
				}
		);
	}
	getTimovi();


	$scope.search2 = function(){
		$scope.pageNum = 0;
		getIgraci();
	}

	$scope.go = function(direction){
		$scope.pageNum += direction;
		getIgraci();
	}

	$scope.delete = function(id){

		var promise = $http.delete(baseUrl + "/" + id);
		promise.then(
				function success(res){
					getIgraci();
				},
				function error(res){
					alert("Something went wrong!");
				}
		)
	}

	$scope.faul= function(id){
		var promise = $http.post(baseUrl + "/" + id);
		promise.then(
				function success(res){
					getIgraci();
				},
				function error(res){
					alert("Something t wrong!");
				}
		)
	}

	$scope.goToAdd = function(id){
		$location.path('/igraci/add/');
	}
	$scope.goToEdit = function(id){
		$location.path('/igraci/edit/' + id);
	}
});
wafepaApp.controller("addCtrl", function($scope, $http, $location){
	
	var baseUrl = "/api/igraci";
	var baseUrlTimovi = "/api/timovi"
	var baseUrlPozicije="/api/pozicije";
	$scope.timovi = [];
	$scope.pozicije = [];
	
	$scope.igrac = {};
	$scope.igrac.imePrezime = "";
	$scope.igrac.broj = "";
	$scope.igrac.licne = "";
	$scope.igrac.pozicijaId = 0;
	$scope.igrac.timId = 0;
	
	
	var getPozicije = function(){
		$http.get(baseUrlPozicije).then(
				function success(res){
					$scope.pozicije = res.data;
				},
				function error(res){
					alert("Nisu se dobavile pozicije!");
				}
		);
	}
	getPozicije();
	
	var getTimovi= function(){
		$http.get(baseUrlTimovi).then(
				function success(res){
					$scope.timovi = res.data;
				},
				function error(res){
					alert("Nisu se dobavili timovi!");
				}
		);
	}
	getTimovi();
	
	
	$scope.add = function(){
		
		$http.post(baseUrl, $scope.igrac).then(
				function success(res){
					$location.path('/igraci');
					
				},
				function error(res){
					alert("Neuspesno dodavanje!");
				}
		);
	}

});
wafepaApp.controller("editCtrl", function($scope, $http, $location,$routeParams){
	var baseUrl = "/api/igraci/";
	var baseUrlTimovi = "/api/timovi"
	var baseUrlPozicije="/api/pozicije";
	var id = $routeParams.aid;
	//edit
	$scope.ed = {};
	$scope.ed.id = "";
	$scope.ed.imePrezime = "";
	$scope.ed.broj = "";
	$scope.ed.timId = 0;
	$scope.ed.pozicijaId = 0;
	$scope.timovi = [];
	$scope.pozicije = [];
	
	var getIgrac= function(){
		$http.get(baseUrl + "/" + id).then(
				function success(res){
					$scope.ed = res.data;
					console.log($scope.ed);
				},
				function error(res){
					alert("Nije se dobavio igrac!");
				}
		);
	}
	getIgrac();
	
	var getPozicije = function(){
		$http.get(baseUrlPozicije).then(
				function success(res){
					$scope.pozicije = res.data;
				},
				function error(res){
					alert("Nisu se dobavile pozicije!");
				}
		);
	}
	getPozicije();
	
	var getTimovi= function(){
		$http.get(baseUrlTimovi).then(
				function success(res){
					$scope.timovi = res.data;
				},
				function error(res){
					alert("Nisu se dobavili timovi!");
				}
		);
	}
	getTimovi();
		
	
	
	

	$scope.edit = function(){
		$http.put(baseUrl + id, $scope.ed).then(
				function success(res){
					 alert("Uspešno izmenjen igrač!");
					$location.path('/igraci');
				},
				function error(res){
					alert("Neuspesna izmena!");
				}
		);
	}
	
	

});

wafepaApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl:'/app/html/home.html'
	})
	.when('/igraci', {
		templateUrl : '/app/html/igraci.html'
	})

	.when('/igraci/add', {
		templateUrl : '/app/html/add.html'
	})
	.when('/igraci/edit/:aid', {
		templateUrl : '/app/html/edit.html'
	})
	.otherwise({
		redirectTo: '/'
	});
}]);