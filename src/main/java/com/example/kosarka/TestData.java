package com.example.kosarka;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.kosarka.model.Igrac;
import com.example.kosarka.model.Pozicija;
import com.example.kosarka.model.Tim;
import com.example.kosarka.service.*;

@Component
public class TestData {

	@Autowired
	private TimService tService;
	@Autowired
	private PozicijaService pService;
	@Autowired
	private IgracService iService;
	
	@PostConstruct
	public void init() {
		Tim tim1 = new Tim();
		tim1.setIme("Beli");
		tim1.setBoje("bela");
		tim1.setTrener("Trener1");
		
		Tim tim2 = new Tim();
		tim2.setIme("Crveni");
		tim2.setBoje("crvena");
		tim2.setTrener("Trener2");
		
		tService.save(tim1);
		tService.save(tim2);
		
		Pozicija pozicija1 = new Pozicija();
		pozicija1.setNaziv("leva");
		Pozicija pozicija2 = new Pozicija();
		pozicija2.setNaziv("desna");
		
		pService.save(pozicija1);
		pService.save(pozicija2);
		
		Igrac igrac = new Igrac();
		igrac.setBroj(2);
		igrac.setImePrezime("Pera SuperGenije");
		igrac.setLicne(2);
		igrac.setPozicija(pozicija2);
		igrac.setTim(tim1);
		igrac.setVanIgre(true);
		
		Igrac igrac1 = new Igrac();
		igrac1.setBroj(12);
		igrac1.setImePrezime("Zika SuperGenije");
		igrac1.setLicne(3);
		igrac1.setPozicija(pozicija2);
		igrac1.setTim(tim1);
		igrac1.setVanIgre(false);
		
		Igrac igrac2 = new Igrac();
		igrac2.setBroj(12);
		igrac2.setImePrezime("Sima Nije Genije");
		igrac2.setLicne(4);
		igrac2.setPozicija(pozicija1);
		igrac2.setTim(tim2);
		igrac2.setVanIgre(true);
		
		iService.save(igrac);
		iService.save(igrac1);
		
	}
}
