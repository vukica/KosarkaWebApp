package com.example.kosarka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.kosarka.dto.PozicijaDTO;
import com.example.kosarka.model.Pozicija;

@Component
public class PozicijaToPozicijaDTO implements Converter<Pozicija, PozicijaDTO> {

	@Override
	public PozicijaDTO convert(Pozicija p) {
		PozicijaDTO dto = new PozicijaDTO();
		dto.setId(p.getId());
		dto.setNaziv(p.getNaziv());
		return dto;
	}
	public List<PozicijaDTO> convert(List<Pozicija> pp){
		List<PozicijaDTO> dtos = new ArrayList<>();
		for(Pozicija p : pp) {
			dtos.add(convert(p));
		}
		return dtos;
	}
}
