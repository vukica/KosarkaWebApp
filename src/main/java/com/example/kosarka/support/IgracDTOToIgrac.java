package com.example.kosarka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.kosarka.dto.IgracDTO;
import com.example.kosarka.model.Igrac;
import com.example.kosarka.repository.IgracRepository;
import com.example.kosarka.repository.PozicijaRepository;
import com.example.kosarka.repository.TimRepository;

@Component
public class IgracDTOToIgrac implements Converter<IgracDTO, Igrac> {
	@Autowired
	private IgracRepository iRep;
	@Autowired
	private TimRepository tRep;
	@Autowired
	private PozicijaRepository pRep;
	@Override
	public Igrac convert(IgracDTO dto) {
		Igrac i = null;

		if(dto.getId() == null) {
		i = new Igrac();
	}else {
		i = iRep.findOne(dto.getId());
	}

	i.setId(dto.getId());
	i.setBroj(dto.getBroj());
	i.setLicne(dto.getLicne());
	i.setImePrezime(dto.getImePrezime());
	i.setTim(tRep.findOne(dto.getTimId()));
	i.setPozicija(pRep.findOne(dto.getPozicijaId()));
	return i;
}
public List<Igrac> convert(List<IgracDTO> dtos){
	List<Igrac> lista = new ArrayList<>();
	for(IgracDTO d : dtos) {
		lista.add(convert(d));
	}
	return lista;
}
}
