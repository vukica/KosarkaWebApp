package com.example.kosarka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.kosarka.dto.TimDTO;
import com.example.kosarka.model.*;

@Component
public class TimToTimDTO implements Converter<Tim, TimDTO>{

	@Override
	public TimDTO convert(Tim t) {
TimDTO dto = new TimDTO();
dto.setIme(t.getIme());
dto.setId(t.getId());
dto.setTrener(t.getTrener());
dto.setBoje(t.getBoje());
		return dto;
	}
public List<TimDTO> convert(List<Tim> timovi){
	List<TimDTO> dtos = new ArrayList<>();
	for(Tim t : timovi) {
		dtos.add(convert(t));
	}
	return dtos;
}
}
