package com.example.kosarka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.kosarka.dto.IgracDTO;
import com.example.kosarka.model.Igrac;

@Component
public class IgracToIgracDTO implements Converter<Igrac, IgracDTO>{

	@Override
	public IgracDTO convert(Igrac i) {
		IgracDTO dto = new IgracDTO();
		dto.setId(i.getId());
		dto.setBroj(i.getBroj());
		dto.setImePrezime(i.getImePrezime());
		dto.setLicne(i.getLicne());
		dto.setPozicijaId(i.getPozicija().getId());
		dto.setPozicijaNaziv(i.getPozicija().getNaziv());
		dto.setTimIme(i.getTim().getIme());
		dto.setTimBoje(i.getTim().getBoje());
		dto.setTimId(i.getTim().getId());
		dto.setTimTrener(i.getTim().getTrener());
		return dto;
	}
	public List<IgracDTO> convert(List<Igrac> lista){
		List<IgracDTO> dtos = new ArrayList<>();
		for(Igrac i : lista) {
			dtos.add(convert(i));
		}
		return dtos;
	}
}
