package com.example.kosarka.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.kosarka.dto.IgracDTO;
import com.example.kosarka.dto.UnosDTO;
import com.example.kosarka.model.Igrac;
import com.example.kosarka.service.IgracService;
import com.example.kosarka.support.IgracDTOToIgrac;
import com.example.kosarka.support.IgracToIgracDTO;
import com.example.kosarka.support.UnosDTOToIgrac;


@RestController
@RequestMapping(value = "/api/igraci")
public class ApiIgracController {


		@Autowired
		private IgracService iService;

		@Autowired
		private IgracDTOToIgrac toIgrac;

		@Autowired
		private IgracToIgracDTO toDto;
		
		@Autowired UnosDTOToIgrac unosToIgrac;

		@RequestMapping(method=RequestMethod.GET)
		public ResponseEntity<List<IgracDTO>> get(
				@RequestParam(required=false) String imePrezime,
				@RequestParam(required=false) Integer broj,
				@RequestParam(required=false) Long timId,
				@RequestParam(defaultValue="0") int pageNum){
			
			Page<Igrac> igraci;
			if(imePrezime != null || broj != null || timId != null) {
				igraci = iService.pretraga(imePrezime,broj,timId, pageNum);
				System.out.println("usao u pretragu");
			}else{
				igraci = iService.findAll(pageNum);
			}
			HttpHeaders headers = new HttpHeaders();
			headers.add("totalPages", Integer.toString(igraci.getTotalPages()) );
			return  new ResponseEntity<>(
					toDto.convert(igraci.getContent()),
					headers,
					HttpStatus.OK);
		}

		@RequestMapping(value="/{id}", method=RequestMethod.GET)
		public ResponseEntity<IgracDTO> get(
				@PathVariable Long id){

			Igrac igrac = iService.findOne(id);

			if(igrac== null){
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<>(
					toDto.convert(igrac),
					HttpStatus.OK);
		}
		@RequestMapping(method=RequestMethod.POST)
		public ResponseEntity<IgracDTO> add(
				@Validated
				@RequestBody UnosDTO noviIgrac){
			System.out.println("usao u  kontroler");
			Igrac igrac = unosToIgrac.convert(noviIgrac); 
			Igrac savedIgrac = iService.save(igrac);

			return new ResponseEntity<>(toDto.convert(savedIgrac),
					HttpStatus.CREATED);
		}
		@RequestMapping(method=RequestMethod.POST,value="/{id}")
		public ResponseEntity<IgracDTO> faul(
				@PathVariable Long id){
			Igrac igrac = iService.findOne(id);
			if(igrac == null) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			iService.faul(igrac.getLicne()+1, id);

			return new ResponseEntity<>(toDto.convert(igrac),
					HttpStatus.OK);
		}

		@RequestMapping(method=RequestMethod.PUT,
				value="/{id}")
		public ResponseEntity<IgracDTO> edit(
				@Validated
				@PathVariable Long id,
				@RequestBody IgracDTO izmenjen){

			
			
			Igrac igrac = toIgrac.convert(izmenjen); 
			System.out.println(igrac.getId() + " " + igrac.getImePrezime());
			Igrac saved = iService.save(igrac);

			return new ResponseEntity<>(toDto.convert(saved),
					HttpStatus.OK);
		}

		@RequestMapping(method=RequestMethod.DELETE,
				value="/{id}")
		public ResponseEntity<IgracDTO> delete(@PathVariable Long id){
			iService.remove(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

