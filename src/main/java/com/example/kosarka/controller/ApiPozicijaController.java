package com.example.kosarka.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.kosarka.dto.PozicijaDTO;
import com.example.kosarka.dto.TimDTO;
import com.example.kosarka.model.Pozicija;
import com.example.kosarka.model.Tim;
import com.example.kosarka.repository.PozicijaRepository;
import com.example.kosarka.service.PozicijaService;
import com.example.kosarka.service.TimService;
import com.example.kosarka.support.PozicijaToPozicijaDTO;
import com.example.kosarka.support.TimToTimDTO;

@RestController
@RequestMapping(value = "/api/pozicije")
public class ApiPozicijaController {
	@Autowired
	private PozicijaService pService;
	@Autowired
	private PozicijaToPozicijaDTO toDto;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<PozicijaDTO>> get(){
		
		List<Pozicija> pp = pService.findAll();
		
		return new ResponseEntity<>(
				toDto.convert(pp), 
				HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<PozicijaDTO> get(
			@PathVariable Long id){
		
		Pozicija p = pService.findOne(id);

		if(p == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(
				toDto.convert(p), 
				HttpStatus.OK);
	}
	
}
