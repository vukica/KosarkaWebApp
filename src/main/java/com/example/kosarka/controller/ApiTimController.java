package com.example.kosarka.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.kosarka.dto.TimDTO;
import com.example.kosarka.model.Tim;
import com.example.kosarka.service.TimService;
import com.example.kosarka.support.TimToTimDTO;


@RestController
@RequestMapping(value = "/api/timovi")
public class ApiTimController {
	
	@Autowired
	private TimService tService;
	@Autowired
	private TimToTimDTO toDto;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<TimDTO>> get(){
		
		List<Tim> tims = 
				tService.findAll();
		
		return new ResponseEntity<>(
				toDto.convert(tims), 
				HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<TimDTO> get(
			@PathVariable Long id){
		
		Tim tim = tService.findOne(id);

		if(tim == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(
				toDto.convert(tim), 
				HttpStatus.OK);
	}
	
}
