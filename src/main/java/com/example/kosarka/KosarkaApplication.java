package com.example.kosarka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import com.example.kosarka.TestData;
@SpringBootApplication
public class KosarkaApplication extends SpringBootServletInitializer {
	@Autowired 
	private TestData td;
	public static void main(String[] args) {
		SpringApplication.run(KosarkaApplication.class, args);
	}
}
