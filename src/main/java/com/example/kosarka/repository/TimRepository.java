package com.example.kosarka.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.kosarka.model.Tim;
@Repository
public interface TimRepository extends JpaRepository<Tim, Long>{

}
