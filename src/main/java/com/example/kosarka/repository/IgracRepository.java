package com.example.kosarka.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.kosarka.model.Igrac;
@Repository
public interface IgracRepository extends JpaRepository<Igrac, Long>{
	@Query("SELECT a FROM Igrac a WHERE "
			+ "(:imePrezime IS NULL or a.imePrezime like :imePrezime ) AND "
			+ "(:broj IS NULL OR a.broj = :broj  ) AND "
			+ "(:timId IS NULL OR a.tim.id = :timId)"
			)
	Page<Igrac> pretraga(@Param("imePrezime")String imePrezime, 
			@Param("broj")Integer broj,
			@Param("timId") Long timId,
			Pageable pageRequest);
	@Modifying
	@Query("update Igrac ig set ig.licne = ?1 where ig.id = ?2")
	void faul(int licne, Long id);

}
