package com.example.kosarka.service;

import java.util.List;

import com.example.kosarka.model.Pozicija;

public interface PozicijaService {
	Pozicija findOne(Long id);
	List<Pozicija> findAll();
	Pozicija save(Pozicija pozicija);
}
