package com.example.kosarka.service;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import com.example.kosarka.model.Igrac;

public interface IgracService {
	
	Igrac findOne(Long id);
	
	Page<Igrac> findAll(int pageNum);
	
	Igrac save(Igrac igrac);
	
	void remove(Long id);

	void faul(int licne,Long id);
	
	Page<Igrac> findByTimId(Long timId, int pageNum);

	Page<Igrac> pretraga(@Param("imePrezime") String imePrezime, 
			@Param("broj") Integer broj, 
			@Param("timId")Long timId,
			int pageNum);
}
