package com.example.kosarka.service;

import java.util.List;

import com.example.kosarka.model.Tim;

public interface TimService {
	Tim findOne(Long id);
	List<Tim> findAll();
	Tim save(Tim tim);
}
