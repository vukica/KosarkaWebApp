package com.example.kosarka.dto;

public class UnosDTO {
	private Long id;
private String imePrezime;
private int broj;
private Long pozicijaId;
private Long timId;
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getImePrezime() {
	return imePrezime;
}
public void setImePrezime(String imePrezime) {
	this.imePrezime = imePrezime;
}
public int getBroj() {
	return broj;
}
public void setBroj(int broj) {
	this.broj = broj;
}
public Long getPozicijaId() {
	return pozicijaId;
}
public void setPozicijaId(Long pozicijaId) {
	this.pozicijaId = pozicijaId;
}
public Long getTimId() {
	return timId;
}
public void setTimId(Long timId) {
	this.timId = timId;
}

}
