package com.example.kosarka.dto;

public class IgracDTO {
	private Long id;
private String imePrezime;
private int broj;
private int licne;
private boolean vanIgre = false;
private Long pozicijaId;
private String pozicijaNaziv;
private Long timId;
private String timIme;
private String timTrener;
private String timBoje;
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getImePrezime() {
	return imePrezime;
}
public void setImePrezime(String imePrezime) {
	this.imePrezime = imePrezime;
}
public int getBroj() {
	return broj;
}
public void setBroj(int broj) {
	this.broj = broj;
}
public int getLicne() {
	return licne;
}
public void setLicne(int licne) {
	this.licne = licne;
}
public boolean isVanIgre() {
	return vanIgre;
}
public void setVanIgre(boolean vanIgre) {
	this.vanIgre = vanIgre;
}
public Long getPozicijaId() {
	return pozicijaId;
}
public void setPozicijaId(Long pozicijaId) {
	this.pozicijaId = pozicijaId;
}
public String getPozicijaNaziv() {
	return pozicijaNaziv;
}
public void setPozicijaNaziv(String pozicijaNaziv) {
	this.pozicijaNaziv = pozicijaNaziv;
}
public Long getTimId() {
	return timId;
}
public void setTimId(Long timId) {
	this.timId = timId;
}
public String getTimIme() {
	return timIme;
}
public void setTimIme(String timIme) {
	this.timIme = timIme;
}
public String getTimTrener() {
	return timTrener;
}
public void setTimTrener(String timTrener) {
	this.timTrener = timTrener;
}
public String getTimBoje() {
	return timBoje;
}
public void setTimBoje(String timBoje) {
	this.timBoje = timBoje;
}


}
