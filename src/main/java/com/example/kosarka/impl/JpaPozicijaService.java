package com.example.kosarka.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kosarka.model.Pozicija;
import com.example.kosarka.repository.PozicijaRepository;
import com.example.kosarka.service.PozicijaService;
@Service
@Transactional
public class JpaPozicijaService implements PozicijaService{

	@Autowired
	private PozicijaRepository pRep;

	@Override
	public Pozicija findOne(Long id) {
		// TODO Auto-generated method stub
		return pRep.findOne(id);
	}

	@Override
	public List<Pozicija> findAll() {
		// TODO Auto-generated method stub
		return pRep.findAll();
	}

	@Override
	public Pozicija save(Pozicija pozicija) {
		// TODO Auto-generated method stub
		return pRep.save(pozicija);
	}
	
	
}
