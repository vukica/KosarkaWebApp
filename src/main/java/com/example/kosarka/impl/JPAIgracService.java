package com.example.kosarka.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.kosarka.model.Igrac;
import com.example.kosarka.repository.IgracRepository;
import com.example.kosarka.service.IgracService;

@Service
@Transactional
public class JPAIgracService implements IgracService{
@Autowired
private IgracRepository irep;
	@Override
	public Igrac findOne(Long id) {
		return irep.findOne(id);
	}

	@Override
	public Page<Igrac> findAll(int pageNum) {
		return irep.findAll(new PageRequest(pageNum, 2));
	}

	@Override
	public Igrac save(Igrac igrac) {
		// TODO Auto-generated method stub
		return irep.save(igrac);
	}

	@Override
	public void remove(Long id) {

		irep.delete(id);
	}

	@Override
	public Page<Igrac> findByTimId(Long timId, int pageNum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Igrac> pretraga(String imePrezime, Integer broj, Long timId, int pageNum) {
		if(imePrezime != null ){
			imePrezime = "%" + imePrezime + "%";
			System.out.println("dodao na model");
		}
		return irep.pretraga(imePrezime, broj, timId, new PageRequest(pageNum, 5));
	}

	@Override
	public void faul(int licne, Long id) {
		irep.faul(licne, id);
		
	}
}


