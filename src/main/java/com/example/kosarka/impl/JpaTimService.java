package com.example.kosarka.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kosarka.model.Tim;
import com.example.kosarka.repository.TimRepository;
import com.example.kosarka.service.TimService;
@Service
@Transactional
public class JpaTimService implements TimService {
	@Autowired
	private TimRepository tRep;
	@Override
	public Tim findOne(Long id) {
		// TODO Auto-generated method stub
		return tRep.findOne(id);
	}

	@Override
	public List<Tim> findAll() {
		// TODO Auto-generated method stub
		return tRep.findAll();
	}

	@Override
	public Tim save(Tim tim) {
		// TODO Auto-generated method stub
		return tRep.save(tim);
	}

}
