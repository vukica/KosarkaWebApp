package com.example.kosarka.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Igrac {
	@Id
	@GeneratedValue
	@Column
private Long id;
	@Column(nullable=false)
private String imePrezime;
	@Column(nullable=false)
private int broj;
	@Column(nullable=false)
private int licne;
	@Column
private boolean vanIgre = false;
	@ManyToOne(fetch=FetchType.EAGER)
private Pozicija pozicija;
	@ManyToOne(fetch=FetchType.EAGER)
private Tim tim;
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getImePrezime() {
	return imePrezime;
}
public void setImePrezime(String imePrezime) {
	this.imePrezime = imePrezime;
}
public int getBroj() {
	return broj;
}
public void setBroj(int broj) {
	this.broj = broj;
}
public int getLicne() {
	return licne;
}
public void setLicne(int licne) {
	this.licne = licne;
	if(licne==5) {
		this.setVanIgre(true);
	}
}
public boolean isVanIgre() {
	return vanIgre;
}
public void setVanIgre(boolean vanIgre) {
	this.vanIgre = vanIgre;
}
public Pozicija getPozicija() {
	return pozicija;
}
public void setPozicija(Pozicija pozicija) {
	this.pozicija = pozicija;
	if(pozicija!=null && !pozicija.getIgraci().contains(this)){
		pozicija.getIgraci().add(this);
	}
}
public Tim getTim() {
	return tim;
}
public void setTim(Tim tim) {
	this.tim = tim;
	if(tim!=null && !tim.getIgraci().contains(this)){
		tim.getIgraci().add(this);
	}
}

}
